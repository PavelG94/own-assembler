#include "virtual_machine.h"
#include <QVector>
#include <QDebug>

VirtualMachine::VirtualMachine(BaseUi *ui, int memoryCellCount)
    : ui(ui)
{
    if (memoryCellCount <= 0) {
        memory.resize(1 + memoryCellCount + defRegCount);
    } else {
        memory.resize(1 + defMemoryCellCount + defRegCount);
    }
}

void VirtualMachine::setupUi(BaseUi *ui)
{
    this->ui = ui;
}

void VirtualMachine::setProgram(const QVector<word_t> &commList)
{
    commandList = commList;
}

bool VirtualMachine::exec()
{
    if (ui == nullptr) {
        return false;
    }
    clear();
    int last = commandList.size() - 1;
    while (instrPtr <= last) {
        word_t commandCode = commandList[instrPtr];
        CommandType commandType = getCommandType(commandCode);
        if (commandType == LABEL) {
            ++instrPtr;
            continue;
        }
        Argument arg1 = getArgument(commandCode, 1);
        Argument arg2 = getArgument(commandCode, 2);
        bool isCorrect = checkCompliance(commandType, arg1.type, arg2.type);
        if (!isCorrect) {
            QString msg = QString("Несоответствие между типом команды(%1)"
                                  "и типами аргументов(%2,%3)")
                                  .arg(typeToString(commandType))
                                  .arg(typeToString(arg1.type))
                                  .arg(typeToString(arg2.type));
            ui->print(msg);
            break;
        }
        if (isUnary(commandType)) {
            bool ok = execUnary(commandType, arg1);
            if (!ok) {
                //сообщение (выводится из execUnary())
                return false;
            }

        } else if (isBinary(commandType)) {
            bool ok = execBinary(commandType, arg1, arg2);
            if (!ok) {
                //сообщение (выводится из execUnary())
                return false;
            }

        } else if (commandType == END) {
            if (instrPtr != last) {
                QString msg = QString("Ошибка: команда %1 не является последней строкой программы.")
                                        .arg(typeToString(commandType));
                ui->print(msg);
                return false;
            }
            ++instrPtr;
        }
    }
    return true;
}

void VirtualMachine::clear()
{
    instrPtr = 0;
    flags = Flags();
    for (int i = 0; i < memory.size(); ++i) {
        memory[i] = 0;
    }
}

bool VirtualMachine::checkCompliance(CommandType commandType, const ArgumentType &argType1, const ArgumentType &argType2)
{
    bool res = bool(argType1 != NUM
                    || commandType == IFN
                    || commandType == IFZ
                    || commandType == JMP
                    || commandType == PRINT);
    return res;
}

bool VirtualMachine::checkRangeHit(const ArgumentType &argType, const DataElement &dataElement)
{
    bool res = true;
    if (argType == REG) {
        res = bool(g_minRegNum <= dataElement && dataElement <= g_maxRegNum);

    } else if (argType == MEMORY_CELL) {
        res = bool(g_minMemoryCellNum <= dataElement && dataElement <= g_maxMemoryCellNum);

    } else if (argType == INDIRECT_ADDR) {
        res = bool((g_minRegNum <= dataElement && dataElement <= g_maxRegNum)
                    || (g_minMemoryCellNum <= dataElement && dataElement <= g_maxMemoryCellNum));

    } else if (argType == NUM) {
        res = bool(g_minDecNum <= dataElement && dataElement <= g_maxDecNum);

    } else {
        //ошибка
        res = false;
    }
    return res;
}

bool VirtualMachine::checkJumpValue(const DataElement &dataElement)
{
    bool res = bool (0 <= dataElement && dataElement < commandList.size());
    return res;
}

DataElement &VirtualMachine::getOperandRef(Argument &arg)
{
    if (arg.type == REG || arg.type == MEMORY_CELL) {
        return memory[arg.value];
    } else if (arg.type == INDIRECT_ADDR) {
        return memory[memory[arg.value]];
    } else {
        //число
        return arg.value;
    }
}

bool VirtualMachine::getUserDecision(const QString &msg)
{
    bool ok = ui->print(msg);
    return ok;
}

CommandType VirtualMachine::getCommandType(word_t commandCode)
{
    static const int mask = 0x3F;                       //6 младших бит
    int type = commandCode >> (2 * (g_argTypeBitSize + g_argValBitSize));
    type &= mask;
    return CommandType(type);
}

bool VirtualMachine::isUnary(CommandType type)
{
    bool result = bool(type == INC || type == DEC || type == NOT
                    || type == IFN || type == IFZ || type == JMP
                    || type == INPUT || type == PRINT || type == CLEAR);
    return result;
}

bool VirtualMachine::execUnary(CommandType commandType, Argument &arg)
{
    DataElement &operandRef = getOperandRef(arg);
    if (checkRangeHit(arg.type, arg.value) == false) {
        QString msg = QString("Выход за пределы допустимого диапазона значений"
                              " для команды %1 со значением аргумента %2")
                              .arg(typeToString(commandType)).arg(valueToString(arg));
        ui->print(msg);
        return false;
    }
    switch (commandType) {
    case INC:
        ++operandRef;
        break;
    case DEC:
        --operandRef;
        break;
    case NOT:
        operandRef = ~operandRef;
        break;
    case IFN:
        if (flags.negative == true) {
            if (checkJumpValue(operandRef) == false) {
                QString msg = QString("Команда %1: значение аргумента %2 вне допустимого диапазона")
                                .arg(typeToString(commandType))
                                .arg(valueToString(arg));
                ui->print(msg);
                return false;
            }
            instrPtr = operandRef;
            //INFO: сразу пропускаю 2ую проверку и увеличение instrPtr
            return true;
        }
        break;
    case IFZ:
        if (flags.zero == true) {
            if (checkJumpValue(operandRef) == false) {
                QString msg = QString("Команда %1: аргумент %2 вне допустимого диапазона")
                                .arg(typeToString(commandType))
                                .arg(valueToString(arg));
                ui->print(msg);
                return false;
            }
            instrPtr = operandRef;
            return true;
        }
        break;
    case JMP:
        if (checkJumpValue(operandRef) == false) {
            QString msg = QString("Команда %1: аргумент %2 вне допустимого диапазона")
                    .arg(typeToString(commandType))
                    .arg(valueToString(arg));
            ui->print(msg);
            return false;
        }
        instrPtr = operandRef;
        return true;
    case INPUT:
        ui->input(QString("Введите значение в \"%1\" %2")
                           .arg(typeToString(arg.type))
                           .arg(valueToString(arg)),
                            operandRef);
        break;
    case PRINT:
    {
        QString msg = QString("Тип: %1 | Имя: %2 | Значение: %3")
                        .arg(typeToString(arg.type)).arg(valueToString(arg)).arg(operandRef);
        ui->print(msg);
        break;
    }
    case CLEAR:
        operandRef = 0;
        break;
    default:
        qDebug() << Q_FUNC_INFO << "Type doesn't represent unary operation";
    }
    //INFO: 2ая проверка
    if (checkRangeHit(NUM, operandRef) == false) {
        QString msg = QString("Выход за пределы допустимого диапазона значений"
                              " после выполнения команды %1 со значением аргумента %2")
                              .arg(typeToString(commandType)).arg(valueToString(arg));
        ui->print(msg);
        return false;
    }
    ++instrPtr;         //INFO: перемещение указателя на следующую команду
    return true;
}

bool VirtualMachine::isBinary(CommandType type)
{
    bool result = bool(type != LABEL && type != END && !isUnary(type));
    return result;
}

bool VirtualMachine::execBinary(CommandType commandType,
                                Argument &arg1, Argument &arg2)
{
    DataElement &operand1Ref = getOperandRef(arg1);
    DataElement &operand2Ref = getOperandRef(arg2);

    //INFO: проверка семантики команд исх. программы
    if (checkRangeHit(arg1.type, arg1.value) == false
            || checkRangeHit(arg2.type, arg2.value) == false) {
        QString msg = QString("Выход за пределы допустимого диапазона значений"
                              " для команды %1 со значениеми аргументов %1, %2")
                              .arg(typeToString(commandType)).arg(operand1Ref).arg(operand2Ref);
        ui->print(msg);
        return false;
    }

    switch (commandType) {
    case MOVE:
        operand1Ref = operand2Ref;
        break;
    case ADD:
        operand1Ref += operand2Ref;
        break;
    case SUB:
        operand1Ref -= operand2Ref;
        break;
    case MULT:
        operand1Ref *= operand2Ref;
        break;
    case DIV:
        if (operand2Ref == 0) {
            QString msg = QString("Попытка деления на ноль.");
            ui->print(msg);
            return false;
        }
        operand1Ref /= operand2Ref;
        break;
    case AND:
        operand1Ref &= operand2Ref;
        break;
    case OR:
        operand1Ref |= operand2Ref;
        break;
    case XOR:
        operand1Ref ^= operand2Ref;
        break;
    case CMP:
        flags.negative = bool((operand1Ref - operand2Ref) < 0);
        flags.zero = bool((operand1Ref - operand2Ref) == 0);
        break;
    default:
        //INFO: ничего не происходит: рассмотрены все возможные случаи
        ;
    }
    //INFO: 2ая проверка
    if (checkRangeHit(NUM, operand1Ref) == false) {
        QString msg = QString("Выход за пределы допустимого диапазона значений"
                              " после выполнения команды %1 со значениеми аргументов %1, %2")
                              .arg(typeToString(commandType)).arg(operand1Ref).arg(operand2Ref);
        ui->print(msg);
        return false;
    }

    ++instrPtr;         //INFO: перемещение указателя на следующую команду
    return true;
}

Argument VirtualMachine::getArgument(word_t commandCode, int indexNum)
{
    static const int argCodeMask = 0x1FFFFFFF;  //3 + 26 = 1 + 4 * 7
    static const int argTypeMask = 0x7;         //3 младших бита
    static const int argContentMask = 0x3FFFFFF;          //26 младших бит
    if (indexNum != 1 && indexNum!= 2) {
        return Argument();                      //показатель неверных данных
    }
    word_t argCode;
    if (indexNum == 1) {
        argCode = commandCode >> (g_argTypeBitSize + g_argValBitSize);
    } else {
        argCode = commandCode;
    }
    argCode &= argCodeMask;
    Argument arg;
    arg.type = ArgumentType((argCode >> g_argValBitSize) & argTypeMask);
    DataElement content = argCode & argContentMask;
    arg.value = content;
    return arg;
}
