#ifndef TRANSLATOR_H
#define TRANSLATOR_H

#include <QStringList>
#include <QVector>
#include <QQueue>
#include <QMap>

#include "SharedData.h"

using namespace shared_data;

class Translator
{
public:
    Translator();

    void setProgram(const QStringList& sCommList);
    QVector<word_t> translate();

protected:
    void setCommandNameBits(word_t &destWord, word_t commNameCode) const;
    void setFirstArgBits(word_t &destWord, word_t argCode) const;
    void setSecondArgBits(word_t &destWord, word_t argCode) const;

    word_t getRegCode(const QString& word);
    word_t getMCellCode(const QString& word);
    word_t getDecNumCode(const QString& word);
    word_t getIAddrCode(const QString& word);

    word_t getProgLineCode();
    word_t getCommNameCode(const QString &word);
    word_t getArgCode();

private:
    QMap<QString,int> m_labelData;

    QStringList m_strCommList;
    QQueue<QString> m_commWordsQueue;
};

#endif // TRANSLATOR_H
