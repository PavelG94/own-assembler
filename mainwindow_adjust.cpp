#include "mainwindow.h"
#include "ui_mainwindow.h"

#include <QString>
#include <QTextEdit>
#include <QAction>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    adjustWindow();

    virtualMachine.setupUi(this);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::createActions()
{
    loadProgramAct = new QAction(tr("Загрузить программу"),this);
    translateAct = new QAction(tr("Транслировать"),this);
    execAct = new QAction(tr("Выполнить"),this);

    connect(loadProgramAct, SIGNAL(triggered()), this, SLOT(loadProgramSlot()));
    connect(translateAct, SIGNAL(triggered()), this, SLOT(translateSlot()));
    connect(execAct, SIGNAL(triggered()), this, SLOT(execSlot()));

    ui->menuBar->addAction(loadProgramAct);
    ui->menuBar->addAction(translateAct);
    ui->menuBar->addAction(execAct);
}

void MainWindow::adjustWindow()
{
    createActions();

    textEdit = new QTextEdit(tr("Текст программы"), this);
    this->setCentralWidget(textEdit);
    this->setWindowTitle(" ");
}
