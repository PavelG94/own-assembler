#include "Debug.h"
#include "SharedData.h"

#include "Translator.h"

Translator::Translator() { }

void Translator::setProgram(const QStringList& sCommList)
{
    m_strCommList = sCommList;
    m_labelData.clear();
    m_commWordsQueue.clear();
}

QVector<word_t> Translator::translate()
{
    int commandNumber{0};
    for (int i = 0; i < m_strCommList.size(); ++i) {
        QString progLineStr = m_strCommList[i].toLower();
        bool isCommLine = ProgramSyntax::instance().isProgramLine(m_strCommList[i].toLower());
        if (isCommLine == false) {
            throw QString("String #%1 \"%2\" doesn't correspond to the command line pattern")
                    .arg(i).arg(m_strCommList[i]);
        }
        //ok, current string corresponds to program line pattern
        QString label = SYNTAX.capLabel(progLineStr);
        QString command = SYNTAX.capCommandName(progLineStr);
        QString firstArg = SYNTAX.capFirstArg(progLineStr);
        QString secondArg = SYNTAX.capSecondArg(progLineStr);

        if (label.isEmpty() && command.isEmpty()
                && firstArg.isEmpty() && secondArg.isEmpty()) {
            //only comment
            continue;
        }
        if (label.isEmpty() == false) {
            //there is a label at the string begin
            m_labelData.insert(label, commandNumber);
        }
        if (command.isEmpty()) {
            //only label
            m_commWordsQueue.enqueue(label);
        } else {
            //full command (unary or binary)
            m_commWordsQueue.enqueue(command);
            if (firstArg.isEmpty() == false)
                m_commWordsQueue.enqueue(firstArg);
            if (secondArg.isEmpty() == false)
                m_commWordsQueue.enqueue(secondArg);
        }
        //strings which consists of comments are ignored in numeration
        commandNumber++;
    }

    QVector<word_t> progLineCodesVec;
    while (m_commWordsQueue.empty() == false) {
        word_t code = getProgLineCode();
        progLineCodesVec.push_back(code);
    }
    return progLineCodesVec;
}

//NOTE: wrapper for not trivial operations
void Translator::setCommandNameBits(word_t &destWord, word_t commNameCode) const
{
    destWord |= (commNameCode << (2 * (g_argTypeBitSize + g_argValBitSize)));
}

void Translator::setFirstArgBits(word_t &destWord, word_t argCode) const
{
    destWord |= (argCode << (g_argTypeBitSize + g_argValBitSize));
}

void Translator::setSecondArgBits(word_t &destWord, word_t argCode) const
{
    destWord |= argCode;
}

word_t Translator::getProgLineCode()
{
    word_t progLineCode(0);
    QString word = m_commWordsQueue.dequeue();
    if (SYNTAX.isCommandName(word) == false) {
        if (m_labelData.contains(word)) {
            return progLineCode;
        } else {
            //error
            throw QString("Word %1 is not a name of command")
                    .arg(word);
        }
    } else {
        CommandType commNameType = SYNTAX.strToType(word);
        setCommandNameBits(progLineCode, commNameType);
        int countArgs = SYNTAX.countArgs(commNameType);
        if (countArgs > 0) {
            word_t argCode = getArgCode();
            setFirstArgBits(progLineCode,argCode);
            if (countArgs > 1) {
                argCode = getArgCode();
                setSecondArgBits(progLineCode,argCode);
            }
        }
        return progLineCode;
    }
}

word_t Translator::getArgCode()
{
    if (m_commWordsQueue.empty()) {
        //error
        throw QString("Not enough arguments.");
    }
    //argument code is |...|argType|argValue|
    word_t argCode(0);
    QString str = m_commWordsQueue.dequeue();
    ArgumentType argType;   int argValue;
    SYNTAX.setArgumentInfoFromStr(str,argType,argValue);
    if (argType == ArgumentType::NONE) {
        if (m_labelData.contains(str)) {
            argType = ArgumentType::NUM;
            argValue = m_labelData.value(str);
        } else {
            throw QString("Argument %1 doesn't correspond to any valid type")
                    .arg(str);
        }
    }
    if(argValue < 0) {
        //set proper sign
        argValue = -argValue;
        int negativeSign = 1;
        argValue |= negativeSign << (g_argValBitSize - 1);
    }
    argCode = argType << g_argValBitSize;
    argCode |= argValue;
    return argCode;
}


