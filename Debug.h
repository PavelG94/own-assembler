#ifndef DEBUG
#define DEBUG

#include <iostream>
#include <sstream>

//NOTE: using trick with inline functions
//for avoiding troubles with global objects

inline std::stringstream& out()
{
    static std::stringstream outStream;
    return outStream;
}

void debug();

template <typename Head, typename... Tail>
static void debug(Head H, Tail... T)
{
#ifndef NDEBUG
    out() << H << ' ';
    debug(T...);
#endif
}

#endif // DEBUG

