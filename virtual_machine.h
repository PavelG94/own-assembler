#ifndef VIRTUALMACHINE_H
#define VIRTUALMACHINE_H

#include "SharedData.h"
#include "baseui.h"

#include <QString>
#include <QVector>

class VirtualMachine
{
public:
    static const int defRegCount = 8;
    static const int defMemoryCellCount = 1024;

    VirtualMachine(BaseUi *ui = nullptr, int memoryCellCount = defMemoryCellCount);

    void setupUi(BaseUi *ui);
    void setProgram(const QVector<word_t>& commList);
    bool exec();

private:
    void clear();
    bool checkCompliance(CommandType commandType, const ArgumentType &argType1,
                                                    const ArgumentType &argType2);
    bool checkRangeHit(const ArgumentType &argType, const DataElement &dataElement);
    bool checkJumpValue(const DataElement &dataElement);

    DataElement& getOperandRef(Argument &arg);
    bool getUserDecision(const QString &msg);

    CommandType getCommandType(word_t commandCode);
    Argument getArgument(word_t commandCode, int indexNum);

    bool isUnary(CommandType type);
    bool execUnary(CommandType commandType, Argument &arg);
    bool isBinary(CommandType type);
    bool execBinary(CommandType commandType, Argument &arg1, Argument &arg2);

private:
    BaseUi *ui;
    QVector<word_t> commandList;

    int instrPtr;
    struct Flags
    {
        Flags() : negative(false),
                  zero(false),
                  overflow(false) {}
        bool negative;
        bool zero;
        bool overflow;
    } flags;
    QVector<DataElement> memory;
};

#endif // VIRTUALMACHINE_H
