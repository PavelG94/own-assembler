#ifndef BASEUI_H
#define BASEUI_H

#include "SharedData.h"

using namespace shared_data;

class QString;

class BaseUi
{
public:

    //Ввод: информация для пользователя, ссылка на ячеку с содержимым;
    virtual void input(const QString &request, DataElement &elem) = 0;

    //Вывод: сформированное информационное сообщение.
    //Возврат: ответ пользователя на сообщение "да/нет"
    virtual bool print(const QString &msg) = 0;
};

#endif // BASEUI_H
