#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include "baseui.h"
#include "Translator.h"
#include "virtual_machine.h"

namespace Ui {
class MainWindow;
}

class QTextEdit;
class QAction;

class MainWindow : public QMainWindow, public BaseUi
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);

    void input(const QString &request, DataElement &elem) override;
    bool print(const QString &msg) override;

    ~MainWindow();

private slots:
    void loadProgramSlot();
    void translateSlot();
    void execSlot();

private:
    void createActions();
    void adjustWindow();

private:
    Ui::MainWindow *ui;
    Translator translator;

    VirtualMachine virtualMachine;

    QTextEdit *textEdit;

    QAction *loadProgramAct;
    QAction *translateAct;
    QAction *execAct;
};

#endif // MAINWINDOW_H
