#ifndef CONSTANTS_H
#define CONSTANTS_H

#include <QRegExp>
#include <QString>
#include <QHash>

namespace shared_data {

//NOTE: double is not supported in bitwise operations,
//but sizeof(long long) == 8 byte and it is enough;
using word_t = long long;
using DataElement = int;

const int g_wordBitSize = 64;

const int g_commCodeBitSize = 6;
const int g_argTypeBitSize = 3;
const int g_argValBitSize = 26;

const int g_minMemoryCellNum = 1;
const int g_maxMemoryCellNum = 1024;
const int g_minRegNum = g_maxMemoryCellNum + 1;
const int g_maxRegNum = g_minRegNum + 7;

const int g_maxDecNum = 33554431;   // 2^25 - 1
const int g_minDecNum = -33554431;  // -2^25 + 1

enum CommandType
{
    INVALID = 0,
    LABEL, MOVE, INC, DEC, ADD, SUB,
    MULT, DIV, NOT, AND, OR, XOR,
    CMP, IFN, IFZ, JMP, INPUT, PRINT,
    CLEAR, END
};

enum ArgumentType
{
    NONE = 0,
    REG, MEMORY_CELL, INDIRECT_ADDR, NUM
};

struct Argument
{
    Argument(ArgumentType type = NONE, int value = 0)
        : type (type), value(value) { }

    ArgumentType type;

    /*
         Для число - его значение;
         Для регистра, яч. памяти и косв. адр. - номер ячейки;
    */
    int value;
};

class ProgramSyntax //Singleton
{
public:
    static ProgramSyntax& instance();

    bool isCommandName(const QString &str);
    int countArgs(CommandType type);

    void addCommand(const QString &commandStr, CommandType type);
    CommandType strToType(const QString &commandNameStr);

    void addArgument(ArgumentType type, const QString &argStr);
    QString typeToStr(ArgumentType type);

    bool isProgramLine(const QString &str);
    void setArgumentInfoFromStr(const QString &str, ArgumentType &typeRes, int &valueRes);

    QString capLabel(const QString &progLineStr);
    QString capCommandName(const QString &progLineStr);
    QString capFirstArg(const QString &progLineStr);
    QString capSecondArg(const QString &progLineStr);

protected:
    ProgramSyntax();
    ProgramSyntax(const ProgramSyntax& other) = delete;
    ProgramSyntax& operator = (const ProgramSyntax& other) = delete;

private:
    void initCommandDataStorage();
    void initArgDataStorage();
    void createRegExpPatterns();

    QHash<QString,CommandType> commandDataStorage;
    QHash<ArgumentType,QString> argDataStorage;

    QString lableRxPattern;
    QString commNameRxPattern;
    QString argRxPattern;
    QString commentRxPattern;
    QString progLineRxPattern;

    QString registerPattern;
    QString memoryCellPattern;
    QString numberPattern;
    QString indirectAddrPattern;

    QRegExp regExp;
};

//NOTE: previous (old) variants for soft integration of new code

QString typeToString(CommandType type);
QString typeToString(ArgumentType type);
QString valueToString(const Argument &arg);

//--------------------------------------------------------------

} //shared_data

//NOTE: #define for convenience
#define SYNTAX shared_data::ProgramSyntax::instance()

#endif // CONSTANTS_H
