#include "SharedData.h"

namespace shared_data {

void ProgramSyntax::initCommandDataStorage()
{
    commandDataStorage["move"] = CommandType::MOVE;
    commandDataStorage["inc"] = CommandType::INC;
    commandDataStorage["dec"] = CommandType::DEC;
    commandDataStorage["add"] = CommandType::ADD;
    commandDataStorage["sub"] = CommandType::SUB;
    commandDataStorage["mult"] = CommandType::MULT;
    commandDataStorage["div"] = CommandType::DIV;
    commandDataStorage["not"] = CommandType::NOT;
    commandDataStorage["and"] = CommandType::AND;
    commandDataStorage["or"] = CommandType::OR;
    commandDataStorage["xor"] = CommandType::XOR;
    commandDataStorage["cmp"] = CommandType::CMP;
    commandDataStorage["ifn"] = CommandType::IFN;
    commandDataStorage["ifz"] = CommandType::IFZ;
    commandDataStorage["jmp"] = CommandType::JMP;
    commandDataStorage["input"] = CommandType::INPUT;
    commandDataStorage["print"] = CommandType::PRINT;
    commandDataStorage["clear"] = CommandType::CLEAR;
    commandDataStorage["end"] = CommandType::END;
}

void ProgramSyntax::initArgDataStorage()
{
    argDataStorage[ArgumentType::REG] = "register";
    argDataStorage[ArgumentType::MEMORY_CELL] = "memory cell";
    argDataStorage[ArgumentType::INDIRECT_ADDR] = "indirect address";
    argDataStorage[ArgumentType::NUM] = "integer number";
}

void ProgramSyntax::createRegExpPatterns()
{
    lableRxPattern = R"((?:(\w+):){0,1})";
    commNameRxPattern = R"(\s*(?:([A-Za-z]+)))";
    argRxPattern = R"((\[\#?\w+\]|\#?[+-]?\w+))";
    commentRxPattern = R"((?:;.+){0,1})";

    const QString argsPart = QString(R"((?:\s+%1(?:\s*,\s*%2)?)?)")
            .arg(argRxPattern).arg(argRxPattern);
    progLineRxPattern = QString(R"(^%1(?:%2%3)?\s*%4)")
            .arg(lableRxPattern).arg(commNameRxPattern)
            .arg(argsPart).arg(commentRxPattern);

    registerPattern = R"(^[abcdefgh]x)";
    memoryCellPattern = R"(^#(\d+))";
    numberPattern = R"(^[+-]?\d+)";
    indirectAddrPattern = R"(^\[\s*#(\d+)|([abcdefgh]x)\s*\])";
}

ProgramSyntax::ProgramSyntax()
{
    initCommandDataStorage();
    initArgDataStorage();
    createRegExpPatterns();
}

ProgramSyntax &ProgramSyntax::instance()
{
    static ProgramSyntax obj;
    return obj;
}

bool ProgramSyntax::isCommandName(const QString &str)
{
    return commandDataStorage.contains(str.toLower());
}

int ProgramSyntax::countArgs(CommandType type)
{
    switch(type) {
    case INVALID: case LABEL:
        return -1;
    case END:
        return 0;
    case INC: case DEC: case NOT: case IFN: case IFZ:
    case JMP: case INPUT: case PRINT: case CLEAR:
        return 1;
    default:
        return 2;
    }
}

void ProgramSyntax::addCommand(const QString &commandStr, CommandType type)
{
    QString strInLower = commandStr.toLower();
    commandDataStorage[strInLower] = type;
}

CommandType ProgramSyntax::strToType(const QString &commandNameStr)
{
    QString strInLower = commandNameStr.toLower();
    return commandDataStorage.contains(strInLower)
           ? commandDataStorage[strInLower]
           : CommandType::INVALID;

}

void ProgramSyntax::addArgument(ArgumentType type, const QString &argStr)
{
    argDataStorage[type] = argStr;
}

QString ProgramSyntax::typeToStr(ArgumentType type)
{
    return argDataStorage.contains(type)
           ? argDataStorage[type]
             : QString();
}

bool ProgramSyntax::isProgramLine(const QString &str)
{
    regExp.setPattern(progLineRxPattern);
    return ( regExp.indexIn(str) != -1 );
}

void ProgramSyntax::setArgumentInfoFromStr(const QString &str, ArgumentType &typeRes, int &valueRes)
{
    regExp.setPattern(registerPattern);
    if (regExp.indexIn(str) != -1) {
        //register
        typeRes = ArgumentType::REG;
        valueRes = g_maxMemoryCellNum + (str[0].toLatin1() - 'a' + 1);
        return;
    }
    regExp.setPattern(memoryCellPattern);
    if (regExp.indexIn(str) != -1) {
        //memory cell
        typeRes = ArgumentType::MEMORY_CELL;
        QString innerStr = regExp.cap(1);
        valueRes = innerStr.toInt();
        if (valueRes < g_minMemoryCellNum
                || valueRes > g_maxMemoryCellNum) {
            throw QString("Memory cell value is out of range");
        }
        return;
    }
    regExp.setPattern(numberPattern);
    if (regExp.indexIn(str) != -1) {
        //integer number
        typeRes = ArgumentType::NUM;
        valueRes = str.toInt();
        if (valueRes < g_minDecNum
                || valueRes > g_maxDecNum) {
            throw QString("Number %1 is out of valid range [%2,%3]")
                    .arg(valueRes).arg(g_minDecNum).arg(g_maxDecNum);
        }
        return;
    }
    regExp.setPattern(indirectAddrPattern);
    if (regExp.indexIn(str) != -1) {
        //indirect address
        typeRes = ArgumentType::INDIRECT_ADDR;
        QString memoryCellInnerStr = regExp.cap(1);
        QString registerInnerStr = regExp.cap(2);
        if (!memoryCellInnerStr.isEmpty()) {
            valueRes = memoryCellInnerStr.toInt();
            if (valueRes < g_minMemoryCellNum
                    || valueRes > g_maxMemoryCellNum) {
                throw QString("Value of memory cell #%1 is out of range.")
                        .arg(memoryCellInnerStr);
            }
        } else {
            valueRes = g_maxMemoryCellNum + (registerInnerStr[0].toLatin1() - 'a' + 1);
        }
        return;
    }
    //further processing takes place in caller
    typeRes = ArgumentType::NONE;
}

QString ProgramSyntax::capLabel(const QString &progLineStr)
{
    //NOTE: QRegExp: first indexIn(), then cap();
    regExp.setPattern(progLineRxPattern);
    if (regExp.indexIn(progLineStr) == -1) {
        return QString();
    }
    return regExp.cap(1);
}

QString ProgramSyntax::capCommandName(const QString &progLineStr)
{
    regExp.setPattern(progLineRxPattern);
    if (regExp.indexIn(progLineStr) == -1) {
        return QString();
    }
    return regExp.cap(2);
}

QString ProgramSyntax::capFirstArg(const QString &progLineStr)
{
    regExp.setPattern(progLineRxPattern);
    if (regExp.indexIn(progLineStr) == -1) {
        return QString();
    }
    return regExp.cap(3);
}

QString ProgramSyntax::capSecondArg(const QString &progLineStr)
{
    regExp.setPattern(progLineRxPattern);
    if (regExp.indexIn(progLineStr) == -1) {
        return QString();
    }
    return regExp.cap(4);
}

//---------------------------------------------

QString typeToString(ArgumentType type)
{
    QString str;
    if (type == REG) {
        str = "регистр";
    } else if (type == MEMORY_CELL) {
        str = "ячейка памяти";
    } else if (type == INDIRECT_ADDR) {
        str = "косвенная адресация";
    } else if (type == NUM) {
        str = "целое десятичное число";
    }
    return str;
}

QString valueToString(const Argument &arg)
{
    if (arg.type == NUM) {
        return QString(arg.value);
    } else if (arg.type == REG) {
        return QString("%1x").arg(QChar('a' + arg.value - g_minRegNum));
    } else if (arg.type == MEMORY_CELL) {
        return QString("#%1").arg(arg.value);
    } else if (arg.type == INDIRECT_ADDR) {
        if (g_minMemoryCellNum <= arg.value && arg.value <= g_maxMemoryCellNum) {
            return QString("[#%1]").arg(arg.value);
        } else if (g_minRegNum <= arg.value && arg.value <= g_maxRegNum) {
            return QString("[%1x]").arg(QChar('a' + arg.value - g_minRegNum));
        }
    }
    return QString();
}

QString typeToString(CommandType type)
{
    QString str;
    switch (type) {
    case LABEL:
        str = "label";
        break;
    case MOVE:
        str = "move";
        break;
    case INC:
        str = "inc";
        break;
    case DEC:
        str = "dec";
        break;
    case ADD:
        str = "add";
        break;
    case SUB:
        str = "sub";
        break;
    case MULT:
        str = "mult";
        break;
    case DIV:
        str = "div";
        break;
    case NOT:
        str = "not";
        break;
    case AND:
        str = "and";
        break;
    case OR:
        str = "or";
        break;
    case XOR:
        str = "xor";
        break;
    case CMP:
        str = "cmp";
        break;
    case IFN:
        str = "ifn";
        break;
    case IFZ:
        str = "ifz";
        break;
    case JMP:
        str = "jmp";
        break;
    case INPUT:
        str = "input";
        break;
    case PRINT:
        str = "print";
        break;
    case CLEAR:
        str = "clear";
        break;
    case END:
        str = "end";
        break;
    case INVALID:
        str = "invalid";
        break;
    }
    return str;
}

}
