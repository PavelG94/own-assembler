#-------------------------------------------------
#
# Project created by QtCreator 2014-10-19T17:07:23
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = OwnAssembler
TEMPLATE = app

CONFIG += c++11

SOURCES += main.cpp\
    mainwindow_adjust.cpp \
    mainwindow.cpp \
    virtual_machine.cpp \
    SharedData.cpp \
    Translator.cpp \
    Debug.cpp

HEADERS  += mainwindow.h \
    baseui.h \
    virtual_machine.h \
    SharedData.h \
    Debug.h \
    Translator.h

FORMS    += mainwindow.ui

OTHER_FILES += \
    test \
    doc/test_output \
    ../../../Build/OwnAssembler/test_program \
    ../../../Build/OwnAssembler/test_program
