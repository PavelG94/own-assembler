#include "mainwindow.h"

#include <QTextEdit>
#include <QFileDialog>
#include <QInputDialog>
#include <QMessageBox>
#include <QStringList>
#include <QVector>

#include <QFile>
#include <QDataStream>
#include <QTextStream>
#include "SharedData.h"

#include <QDebug>

void MainWindow::input(const QString &request, DataElement &elem)
{
    int value = QInputDialog::getInt(this, tr("Запрос ввода"),
                         request, 0, g_minDecNum, g_maxDecNum);
    elem = value;
}

bool MainWindow::print(const QString &msg)
{
    int pressedButton = QMessageBox::information(this, tr(" "),
                             msg, QMessageBox::Ok | QMessageBox::Cancel);
    if (pressedButton == QMessageBox::Cancel) {
        return false;
    } else {
        return true;
    }
}

void MainWindow::loadProgramSlot()
{
    QString fname = QFileDialog::getOpenFileName(this, tr("Файл с текстом программы"),
                                                 QString(),
                                                 tr("Файл *.txt"));
    if (fname.isEmpty() || fname.endsWith(".txt") == false) {
        qDebug() << QString("%1 empty file name").arg(Q_FUNC_INFO);
        return;
    }
    QFile file(fname);
    bool ok = file.open(QIODevice::ReadOnly);
    if (!ok) {
        qDebug() << QString("%1 can't open file for writing").arg(Q_FUNC_INFO);
        return;
    }
    QTextStream in(&file);
    QString text = in.readAll();
    textEdit->setText(text);
}

void MainWindow::translateSlot()
{
    //Сохранение программы в бинарном файле
    QString fname = QFileDialog::getSaveFileName(this, tr("Файл для сохранения"),
                                                 QString(),
                                                 tr("Файл *.bin"));
    if (fname.isEmpty() || fname.endsWith(".bin") == false) {
        qDebug() << QString("%1 empty file name").arg(Q_FUNC_INFO);
        return;
    }

    //Пустые строки сразу игнорируются
    QStringList sCommList = textEdit->toPlainText().split("\n", QString::SkipEmptyParts);

    translator.setProgram(sCommList);
    try {
        QVector<word_t> codeCommList = translator.translate();
        QFile file(fname);
        bool ok = file.open(QIODevice::WriteOnly);
        if (!ok) {
            qDebug() << QString("%1 can't open file for writing").arg(Q_FUNC_INFO);
            return;
        }
        QDataStream sout(&file);
        for (int i = 0; i < codeCommList.size(); ++i) {
            sout << codeCommList[i];
        }

    } catch(const QString& msg) {
        QMessageBox::warning(this, "Ошибка", msg);
        return;
    }
}


void MainWindow::execSlot()
{
    QString fname = QFileDialog::getOpenFileName(this, tr("Файл с программой"),
                                                 QString(),
                                                 tr("Файл *.bin"));
    if (fname.isEmpty()) {
        qDebug() << QString("%1 empty file name").arg(Q_FUNC_INFO);
        return;
    }

    QFile file(fname);
    bool ok = file.open(QIODevice::ReadOnly);
    if (!ok) {
        qDebug() << QString("%1 can't open file for writing").arg(Q_FUNC_INFO);
        return;
    }
    QDataStream in(&file);
    QVector<word_t> commList;
    word_t command;
    while (in.atEnd() == false) {
        in >> command;
        commList.push_back(command);
    }
    virtualMachine.setProgram(commList);
    virtualMachine.exec();
}
